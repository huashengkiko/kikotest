package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.ArticleActionAttribute;
import java.util.*;

public interface ArticleActionAttributeService {

    PageBean<ArticleActionAttribute> findPage(ArticleActionAttribute eo, Integer page, Integer size);

    List<ArticleActionAttribute> findAll(ArticleActionAttribute eo);

    ArticleActionAttribute detail(String pk);

    Boolean update(String pk, ArticleActionAttribute eo);

    Boolean create(ArticleActionAttribute eo);

    Boolean delete(String... pk);
}