package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.ArticleActionLog;
import java.util.*;

public interface ArticleActionLogService {

    PageBean<ArticleActionLog> findPage(ArticleActionLog eo, Integer page, Integer size);

    List<ArticleActionLog> findAll(ArticleActionLog eo);

    ArticleActionLog detail(String pk);

    Boolean update(String pk, ArticleActionLog eo);

    Boolean create(ArticleActionLog eo);

    Boolean delete(String... pk);
}