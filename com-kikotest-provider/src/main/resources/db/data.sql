DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product(id varchar(32) unsigned not null primary key,name varchar(32),price int,tenant_id varchar(32),created_by varchar(32),updated_by varchar(32),created_at date,updated_at date, dr int(1));

INSERT INTO product(id, name, price, tenant_id, created_by, updated_by, created_at, updated_at, dr) VALUES ('1', '苹果', 20, null, null, null, null, null, 0),
('2', '梨', 10, null, null ,null, null, null, 0),
('3', '桃子', 123, null, null ,null, null, null, 0),
('4', '香蕉', 11, null, null ,null, null, null, 0),
('5', '猕猴桃', 50, null, null ,null, null, null, 0),
('6', '桔子', 33, null, null ,null, null, null, 0),
('7', '菠萝', 90, null, null ,null, null, null, 0),
('8', '芒果', 766, null, null ,null, null, null, 0),
('9', '红枣', 29, null, null ,null, null, null, 0),
('10', '榴莲', 990, null, null ,null, null, null, 0),
('11', '柚子', 291, null, null ,null, null, null, 0),
('12', '小番茄', 200, null, null ,null, null, null, 0);
CREATE TABLE IF NOT EXISTS article(
    id varcahr(32) not null primary key,    category_id varchar(255) not null ,    wechat_publist_no varchar(255)  ,    tenant_id varchar(255)  ,    created_at datetime  ,    created_by datetime  ,    updated_by datetime  ,    updated_at datetime  ,    dr tinyint  ,    title varchar(255)  ,    author varchar(255)  ,    status varchar(255)  ,    thumb_media_id varchar(255)  ,    thumb_media_url varchar(255)  ,    digest varchar(255)  ,    show_cover_pic tinyint  ,    content_source_url varchar(255)  ,    need_open_comment tinyint  ,    only_fans_can_comment tinyint  ,    last_updated_time datetime  ,    release_time datetime  ,    ext1 varchar(255)  ,    ext2 varchar(255)  ,    ext3 varchar(255)  ,    ext4 varchar(255)  ,    ext_json varchar(255)  );
CREATE TABLE IF NOT EXISTS article_action_log(
    id varcahr(32) not null primary key,    wechat_publist_no varchar(255)  ,    article_id varchar(255)  ,    tenant_id varchar(255)  ,    union_id varchar(255)  ,    created_at datetime  ,    created_by datetime  ,    updated_by datetime  ,    updated_at datetime  ,    dr tinyint  ,    type varchar(255)  ,    member varchar(255)  ,    recommend_union_id varchar(255)  ,    recommend varchar(255)  ,    user_type varchar(255)  ,    recommend_type varchar(255)  ,    operate_time datetime  ,    ext1 varchar(255)  ,    ext2 varchar(255)  ,    ext3 varchar(255)  ,    ext4 varchar(255)  ,    ext_json varchar(255)  );
CREATE TABLE IF NOT EXISTS article_action_attribute(
    id varcahr(32) not null primary key,    wechat_publist_no varchar(255)  ,    article_id varchar(255)  ,    tenant_id varchar(255)  ,    created_at datetime  ,    created_by datetime  ,    updated_by datetime  ,    updated_at datetime  ,    dr tinyint  ,    type varchar(255)  ,    number int   ,    ext1 varchar(255)  ,    ext2 varchar(255)  ,    ext3 varchar(255)  ,    ext4 varchar(255)  ,    ext_json varchar(255)  );
