package com.kiko.com.test.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kiko.com.test.service.ArticleService;
import com.kiko.com.test.domain.eo.Article;
import com.kiko.com.test.extension.AppRuntimeEnv;
import com.kiko.com.test.mapper.ArticleMapper;
import com.deepexi.util.pageHelper.PageBean;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
@Service(version = "${demo.service.version}")
public class ArticleServiceImpl implements ArticleService {

    private static final Logger logger = LoggerFactory.getLogger(ArticleServiceImpl.class);

    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private AppRuntimeEnv appRuntimeEnv;

    @Override
    public PageBean findPage(Article eo, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<Article> list = articleMapper.findList(eo);
        return new PageBean<>(list);
    }

    @Override
    public List<Article> findAll(Article eo) {
        List<Article> list = articleMapper.findList(eo);
        return list;
    }
    @Override
    public Article detail(String pk) {
        return articleMapper.selectById(pk);
    }

    @Override
    public Boolean create(Article eo) {
        int result = articleMapper.insert(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean update(String pk,Article eo) {
        eo.setId(pk);
        int result = articleMapper.updateById(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean delete(String...pk) {
        int result = articleMapper.deleteByIds(pk);
        if (result > 0) {
            return true;
        }
        return false;
    }

}