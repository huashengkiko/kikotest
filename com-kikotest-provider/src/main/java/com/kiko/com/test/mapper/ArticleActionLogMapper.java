package com.kiko.com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiko.com.test.domain.eo.ArticleActionLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleActionLogMapper extends BaseMapper<ArticleActionLog> {

    List<ArticleActionLog> findList(@Param("eo") ArticleActionLog eo);

    int deleteByIds(String... pks);
}
